from orders.serializers.computer_spare_part import CentralProcessingUnitModelSerializer, VideoCardModelSerializer
from orders.serializers.computer_spare_part_handbook import TypeModelSerializer, CompanyModelSerializer, \
    ImagesCreateUpdateModelSerializer, \
    RatingModelSerializer, CommentsModelSerializer, SalesModelSerializer, BasketModelSerializer, OrderModelSerializer, \
    PaymentsModelSerializer, ImagesListModelSerializer, CommentsListModelSerializer
